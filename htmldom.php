<?php
require "node.php";

class Htmldom
{
    private $root = null;
    private $selectedNodeList = [];

    function __construct(string $string)
    {
        $this->root = new Node($string);
    }

    // public function selectedNode(&$node)
    // {
    //     $this->selectedNode = $node;
    // }

    public function getElementById($id)
    {
        return $node = &$this->root->_indexList["id"]["$id"] ?? false;
        // if(!$node)
        //     return false;
        // $that = clone $this;
        // $that->selectedNode($node);
        // return $that;
    }
    
    public function getElementsByAttr($attr, $value = 0)
    {
        return $node = $this->root->_indexList["attr"][$attr][$value] ?? false;
        // if (!$node)
        //     return false;
        // $that = clone $this;
        // $that->selectedNode($node);
        // return $that;
    }
}