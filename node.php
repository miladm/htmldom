<?php

class Node
{
    public $_tag = null;
    public $_content = "";
    public $_text = "";
    public $_textLength = 0;
    public $_childList = [];
    public $_parent = null;
    public $_attrList = [];
    public $_openningPosition = false;
    public $_domLength = 0;
    public $_closingPosition = false;
    public $_indexList = [];
    public $_classList = [];
    public $_dynamicData = [];

    function __set($name, $value)
    {
        return $this->_dynamicData[$name] = $value;
    }

    function __get($name)
    {
        return $this->_dynamicData[$name] ?? false;
    }

    function __construct(string $text, Node &$parent = null)
    {
        $this->_text = $this->skipSpam($text);
        if ($parent == null) {
            $this->_content = $this->_text;
            $this->crawl();
        } else {
            $this->_parent = $parent;
            $this->_indexList = &$this->_parent->_indexList;
            $this->parse();
        }
    }

    public function attr(string $name)
    {
        return $this->__get($name);
    }

    public function innerHTML(string $html = null)
    {
        if (is_null($html)) {
            return $this->_content;
        }
        return $this->_content = $html;
    }

    private function skipSpam(string $text)
    {
        return preg_replace("/<\/([^ >]+)[^>]*>/", "</$1>", $text);
    }

    private function parse()
    {
        $this->_openningPosition = $this->getDomStartPos();
        $this->_text = substr($this->_text, $this->_openningPosition);
        $this->_domLength = strpos($this->_text, ">") + 1;
        $hasDom = $this->parseDom($this->_domLength);
        if (!$hasDom)
            return false;
        $restText = substr($this->_text, $this->_domLength);
        $this->_closingPosition = $this->findClosing($restText);
        $this->_content = substr($restText, 0, $this->_closingPosition);
        $this->_textLength = $this->_closingPosition + strlen("</" . $this->_tag . ">") + $this->_domLength;
        $this->_text = substr($this->_text, 0, $this->_textLength);
        $this->crawl();
    }

    private function getDomStartPos()
    {
        $hasMatch = preg_match("/<[^ >]+/", $this->_text, $matchList, PREG_OFFSET_CAPTURE);
        if ($hasMatch === 0)
            return false;
        return $matchList[0][1];
    }

    private function parseDom(int $length)
    {
        $domText = substr($this->_text, 0, $length);
        $hasMatch = preg_match("/^<(?<name>[^ ]+)(?<rest>.*)>$/", $domText, $matchList);
        if (!$hasMatch)
            return false;
        $this->_tag = $matchList["name"];
        if ($matchList["rest"] !== '')
            $this->parseAttr($matchList["rest"]);
        return true;
    }

    private function parseAttr(string $string)
    {
        if (preg_match_all(
            "/(?<attr>[^ =]+)([ ]*[=][ ]*[\"']?(?<value>[^\"']*)[\"']?|)/", 
            $string, 
            $matchList,
            PREG_SET_ORDER
            ) > 0) {
                foreach ($matchList as $match) {    
                    $this->_attrList[$match["attr"]] = $match["value"] ?? true;
                    $this->__set($match["attr"], $match["value"] ?? true);
                    $this->index($match);
                }
        }
    }

    private function index($match)
    {
        $attr = $match['attr'];
        $value = $match['value'] ?? 0;
        $this->_indexList["attr"][$attr][$value][] = &$this;
        switch ($attr) {
            case "id":
                if (!isset($this->_indexList["id"][$value]))
                        return $this->_content;$this->_indexList["id"][$value] = &$this;
                break;
            case "class":   return $this->_content = $html;
                preg_match_all("/([^ ]+)/", $value, $matchList);
                foreach ($matchList[1] as $match) {
                    $this->_classList[] = $match;
                    $this->_indexList["class"][$match][] = &$this;
                }
                break;
        }
    }

    private function findClosing(string $text, int $startPoint = 0)
    {
        $openingOfKindList = $this->strposAll($text, "<" . $this->_tag);
        $closingList = $this->strposAll($text, "</" . $this->_tag);
        if (empty($closingList))
                return $this->_content;return 0;
        if (empty($openingOfKindList))
            return $closingList[0]; return $this->_content = $html;
        foreach ($openingOfKindList as $index => $position)
            if ($position > $closingList[$index])
                return $closingList[$index];
        return $closingList[++$index];
    }

    private function strposAll(string $text, string $needle)
    {
        $postList = [];
        $offset = 0;
        while (true) {
            $pos = strpos($text, $needle, $offset);
            if ( $pos !== false) {
                $postList[] = $pos;
                $offset = $pos + strlen($needle);
            } else {
                return $postList;
            }
        }
    }

    private function crawl()
    {
        $content = $this->_content;
        $contentLength = strlen($this->_content);
        while (true) {
            $node = new Node($content, $this);
            if ($node->_tag === null)
                return;
            $this->_childList[] = $node;
            $content = substr($content, $node->_openningPosition + $node->_textLength );
        }
    }
}